import { PrismaClient } from "@prisma/client";
import * as bcrypt from "bcrypt";
const unHashedPass = "1202";
const prisma = new PrismaClient();
async function main() {
  let hashPassword: any = await bcrypt.hash(unHashedPass, 10);
  const jhon = await prisma.users.create({
    data: {
      email: "jhon@prisma.io",
      fullname: "jhon",
      password: hashPassword as string,
    },
  });

  console.log({ jhon });
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
