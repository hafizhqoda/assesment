"use client";
import { useSession } from "next-auth/react";
import { redirect } from "next/navigation";

export default function Home() {
  const { data: session } = useSession();
  if (session) {
    console.log(session);
    return redirect("/task");
  }
  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className=" h-screen flex items-center justify-center p-10 ">
          <div className="">
            <div>
              <Tab
                headerType="Flat"
                listHeader={[
                  {
                    id: 0,
                    title: "Login",
                    icon: "",
                    onClick(params) {
                      () => {
                        console.log("tes");
                      };
                    },
                  },
                  {
                    id: 1,
                    title: "Guest",
                    icon: "",
                    onClick(params) {},
                  },
                ]}
              >
                <div id="step" itemID="0">
                  <div className="w-100">
                    <input
                      type="text"
                      name="type_login"
                      required
                      id="type_login"
                      className="none"
                      value="login"
                    />
                    <div className="mb-2">
                      <label
                        htmlFor="price"
                        className="block text-sm font-medium leading-6 text-gray-900"
                      >
                        Email
                      </label>
                      <div className="relative mt-2 rounded-md shadow-sm">
                        <input
                          type="email"
                          name="email"
                          required
                          id="email"
                          className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          placeholder="example@local.com"
                        />
                      </div>
                    </div>
                    <div className="mb-2">
                      <label
                        htmlFor="password"
                        className="block text-sm font-medium leading-6 text-gray-900"
                      >
                        Password
                      </label>
                      <div className="relative mt-2 rounded-md shadow-sm">
                        <input
                          type="password"
                          required
                          name="password"
                          id="password"
                          className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          placeholder="enter your password"
                        />
                      </div>
                    </div>
                    <div className="relative mt-2 pt-3 rounded-md flex items-center justify-center">
                      <button className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded">
                        Login
                      </button>
                    </div>
                  </div>
                </div>
                <div id="step" itemID="1">
                  <div className="w-100">
                    <input
                      type="text"
                      name="type_login"
                      required
                      id="type_login"
                      className="none"
                      value="guest"
                    />
                    <div className="relative mt-2 pt-3 rounded-md flex items-center justify-center">
                      <button className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded">
                        Login
                      </button>
                    </div>
                  </div>
                </div>
              </Tab>
            </div>
          </div>
        </div>
      </form>
    </>
  );
}
