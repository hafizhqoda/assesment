"use client";
import { empty } from "@prisma/client/runtime/library";
import { signOut, useSession } from "next-auth/react";
import Link from "next/link";

export default function Nav() {
  const { data: session } = useSession();
  return (
    <nav>
      <>
        {session === undefined || session === null ? (
          ""
        ) : (
          <>
            <div className="text-slate-900 gap-4 flex border-b-4 p-5">
              <div className="flex-1 w-32 flex justify-center gap-4">
                <Link href={"task"}>
                  <span className="">Task</span>
                </Link>
                {session?.user?.email && (
                  <Link href={"profile"}>
                    <span className="">Profile</span>
                  </Link>
                )}
              </div>
              <div className="flex-none">
                <button
                  onClick={() => {
                    signOut();
                  }}
                >
                  logout
                </button>
              </div>
            </div>
          </>
        )}
      </>
    </nav>
  );
}
