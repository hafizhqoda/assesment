import { PrismaClient } from "@prisma/client";
import bcrypt from "bcrypt";
async function newlogin(email: any, password: any, res: any = null) {
  try {
    const prisma = new PrismaClient();
    let resp_data, resp_code, res;

    const checkEmail = await prisma.users.findUnique({
      where: {
        email,
      },
    });
    if (checkEmail) {
      let hashPassword: any = await bcrypt.hash(password, 10);
      let checkPass = await bcrypt.compare(password, checkEmail.password);
      if (checkPass) {
        return (res = {
          resp_code: "00",
          resp_data: { ...checkEmail },
          resp_msg: "Berhasil!",
        });
      } else {
        res = {
          resp_code: "03",
          resp_data: [],
          resp_msg: "Password Tidak valid",
        };
        return res;
      }
    } else {
      res = {
        resp_code: "02",
        resp_data: [],
        resp_msg: "Email Tidak valid",
      };
      return res;
    }
  } catch (error: any) {
    console.log(error);
    //  logger.error({ error }, "Error API Get Login:");
    //  return generateResponse({}, "03", error.toString());
    // return res.status(500).send(generateResponse(null, '99', error.toString()));
  }
}

export { newlogin };
