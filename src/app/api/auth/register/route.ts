import { PrismaClient } from "@prisma/client";
import * as bcrypt from "bcrypt";
import { NextResponse } from "next/server";
export async function POST(request: Request) {
  try {
    let res, resp_code, resp_data, resp_msg;
    const { email, password, fullname } = await request.json();
    // console.log({ email, password, fullname });
    const hashPassword = await bcrypt.hash(password, 10);

    const prisma = new PrismaClient();
    const checkemail = await prisma.users.findFirst({
      where: { email },
    });
    if (checkemail) {
      res = {
        status: "02",
        data: [],
        message: "Email Already Exist!",
      };
      // return res;
      return NextResponse.json({ ...res });
    }
    const users = await prisma.users.create({
      data: {
        email,
        fullname,
        password: hashPassword,
      },
    });
    if (users) {
      res = {
        status: "00",
        data: users,
        message: "Create Account Succes!",
      };
    } else {
      res = {
        status: "02",
        data: [],
        message: "Create Account Failed!",
      };
    }
    return NextResponse.json({ ...res });
  } catch (error) {
    return NextResponse.json({ status: 401, message: "Failed" });
  }
}
