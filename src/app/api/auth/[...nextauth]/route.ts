import NextAuth, { NextAuthOptions, getServerSession } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { newlogin } from "../login/login.api";

export const authOptions: NextAuthOptions = {
  secret: process.env.AUTH_SECRET,
  providers: [
    CredentialsProvider({
      // The name to display on the sign in form (e.g. "Sign in with...")
      name: "Credentials",
      // `credentials` is used to generate a form on the sign in page.
      // You can specify which fields should be submitted, by adding keys to the `credentials` object.
      // e.g. domain, username, password, 2FA token, etc.
      // You can pass any HTML attribute to the <input> tag through the object.
      credentials: {
        email: {
          label: "Email",
          type: "email",
          placeholder: "Insert Email Here...",
        },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        var { email, password, type_login } = credentials as any;
        var user: any;
        // Add logic here to look up the user from the credentials supplied
        //   const user = { id: "1", name: "J Smith", email: "jsmith@example.com" };
        user = await newlogin(email, password);
        let userInfo = user.resp_data;
        if (type_login === "guest") {
          userInfo = {
            name: "Guest",
            email: "",
            picture: "",
          };
        } else if (type_login === "login") {
          userInfo = {
            ...userInfo,
            name: userInfo.fullname,
            email: userInfo.email,
            picture: "",
          };
        } else {
          if (user.resp_code === "02") {
            return null;
          }
        }
        return { ...userInfo };
      },
    }),
  ],
  callbacks: {
    async signIn(signInc) {
      return true;
    },
    async redirect({ url, baseUrl }) {
      return baseUrl;
    },

    async jwt({ token, user, session }) {
      // Persist the OAuth access_token to the token right after signin
      // console.log("jwt callback ====", { token, session, user });
      return { ...token, ...user };
    },
    async session({ session, token, user }) {
      // console.log("User in session: ", user);
      // Send properties to the client, like an access_token from a provider.
      // session.accessToken = token.accessToken;
      // console.log("session callback ====", { token, user, session });
      return { ...session, ...user };
    },
  },
  pages: { signIn: "/login" },
  events: {
    async signOut({ session, token }) {
      // await logout((token as any).refresh_token);
    },
  },
  session: {
    strategy: "jwt",
  },
};
const handler = NextAuth(authOptions);
// export default
export { handler as GET, handler as POST };
