import { PrismaClient } from "@prisma/client";
import { NextRequest, NextResponse } from "next/server";

export async function GET(request: NextRequest) {
  try {
    const prisma = new PrismaClient();
    const list_data = await prisma.todo_list.findMany({
      where: {
        status_task: 1,
      },
      orderBy: { id: "desc" },
    });
    const list_data_done = await prisma.todo_list.findMany({
      where: {
        status_task: 2,
      },
      orderBy: { id: "desc" },
    });
    // const tag = request.nextUrl.searchParams.get("task");
    // revalidateTag("task");
    // return NextResponse.json({
    //   data,
    //   revalidated: true,
    // });
    // const list_data = await data;
    return NextResponse.json({
      status: 200,
      message: "Jos",
      list_data,
      list_data_done,
    });
  } catch (error) {
    return NextResponse.json(error);
  }
}
