import { PrismaClient } from "@prisma/client";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";
export async function POST(request: NextRequest) {
  try {
    let res, resp_code, resp_data, resp_msg;
    const tag = request.nextUrl.searchParams;
    const { title } = await request.json();
    const prisma = new PrismaClient();
    const data = await prisma.todo_list.create({
      data: {
        title: title,
        status_task: 1,
      },
    });
    if (data) {
      res = {
        resp_code: "00",
        resp_data: data,
        resp_msg: "Create Task Succes!",
      };
    } else {
      res = {
        resp_code: "02",
        resp_data: [],
        resp_msg: "Create task Failed!",
      };
    }
    //
    revalidatePath("/task");
    return NextResponse.json(res, { status: 200 });
  } catch (error) {
    return NextResponse.json(error);
  }
  //   return NextResponse.json({ message: "Success" });
}
