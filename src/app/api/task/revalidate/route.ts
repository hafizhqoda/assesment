import { PrismaClient } from "@prisma/client";
import { revalidatePath, revalidateTag } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

export async function POST(request: NextRequest) {
  // const tag = request.nextUrl.searchParams;
  // console.log(tag);
  const { title } = await request.json();
  const prisma = new PrismaClient();
  const data = await prisma.todo_list.create({
    data: {
      title: title,
      status_task: 1,
    },
  });
  // revalidateTag(tag as any);
  // revalidatePath("/task");

  return NextResponse.json({ data, revalidate: true, now: Date.now() });
}
