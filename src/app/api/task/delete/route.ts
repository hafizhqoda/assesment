import { PrismaClient } from "@prisma/client";
import { NextRequest, NextResponse } from "next/server";

export async function POST(request: NextRequest) {
  try {
    const prisma = new PrismaClient();
    const { id } = await request.json();
    //  const res = await prisma.todo_list.findUnique({ where: { id } });
    const res = await prisma.todo_list.delete({
      where: { id },
    });
    if (res)
      return NextResponse.json({
        status: 200,
        message: "Jos",
        data: { ...res },
      });
  } catch (error) {
    return NextResponse.json(error);
  }
}
