"use client";
import { useSession } from "next-auth/react";

export default function ProfilPage() {
  const { data: session } = useSession();
  if (session) {
    //logged in code
    // router.push("/task");
  }
  return (
    <>
      <div className="text-slate-900 h-screen flex flex-col items-center ">
        <div>Hello {session?.user?.name}</div>
        <div>Email {session?.user?.email}</div>
      </div>
    </>
  );
}
