import { getTask } from "@/action/controllers";
import TaskComponent from "@/app/(be)/task/task-form";

export default async function Task() {
  const list = await getTask();
  return (
    <>
      <div className="text-slate-900 h-screen flex flex-col items-center  p-10 ">
        <TaskComponent data={list || []} />
      </div>
    </>
  );
}
