"use client";
import {
  addTask,
  deleteTask,
  getTaskId,
  updateStatusTask,
  updateTask,
} from "@/action/controllers";
import { useRef, useState } from "react";
import { useFormStatus } from "react-dom";
import { FaPencilAlt, FaRegCheckCircle, FaTrash } from "react-icons/fa";
import moment from "moment";
import Swal from "sweetalert2";

export function SubmitButton() {
  const { pending } = useFormStatus();
  return (
    <button
      aria-disabled={pending}
      className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded"
    >
      {pending ? "Creating Task... " : "Create Task"}
    </button>
  );
}

export function SubmitButtonEdit() {
  const { pending } = useFormStatus();
  return (
    <>
      <button
        aria-disabled={pending}
        className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded"
      >
        {pending ? "Updating Task ... " : "Update Task"}
      </button>
      <button
        aria-disabled={pending}
        className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded"
      >
        Cancel
      </button>
    </>
  );
}

export default function TaskComponent({ data }: any) {
  //   const [state, formAction] = useFormState(createTodo, initialState);
  const ref = useRef<HTMLFormElement>(null);
  const refKadal = useRef<any>(null);
  const { pending } = useFormStatus();
  const [form, setForm] = useState<any>();
  const handleChange = (e: any) => {
    // setForm({
    //   [e.target.name]: e.target.value,
    // });
    setvalueTitle(e.target.value);
  };

  const [mode, setMode] = useState<any>();
  const [val, setvalueTitle] = useState<string>("");
  const [id, setId] = useState<number>();
  // const { setValue } = useFormContext();

  return (
    <>
      <form
        ref={ref}
        action={async (formData) => {
          if (mode !== "edit") {
            await addTask(formData);
            // ref.current?.reset();
            setvalueTitle("");
            setMode("");
          } else {
            const update = await updateTask(id as number, val as string);
            if ((update?.ress?.status as any) === 200) {
              // ref.current?.reset();
              setvalueTitle("");
              setMode("");
            }
          }
        }}
      >
        <div className="flex flex-col items-center">
          <h1 className="font-semibold text-2xl ">Task Management</h1>
          <div className="w-100 ">
            <div className="mb-2">
              <label
                htmlFor="price"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Title
              </label>
              <div className="relative mt-2 rounded-md shadow-sm">
                <input
                  type="text"
                  name="title"
                  value={val || ""}
                  required
                  onChange={handleChange}
                  id="title"
                  className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  placeholder="Enter Task here ..."
                />
              </div>
            </div>

            <div className="relative mt-2 pt-3 rounded-md flex items-center justify-center">
              {mode === "edit" ? (
                <>
                  <button
                    aria-disabled={pending}
                    className="mr-3 bg-orange-500 hover:bg-blue-700 text-white py-2 px-4 rounded"
                  >
                    {pending ? "Updating Task ... " : "Update Task"}
                  </button>
                  <button
                    aria-disabled={pending}
                    className="bg-slate-500 hover:bg-blue-700 text-white py-2 px-4 rounded"
                    onClick={() => {
                      setMode("add");
                      setvalueTitle("");
                    }}
                  >
                    Cancel
                  </button>
                </>
              ) : (
                <SubmitButton />
              )}
            </div>
          </div>
        </div>
      </form>
      <div className="">
        {/* <Suspense
            fallback={<p className="text-slate-900">Loading weather...</p>}
          >
            <DataList />
          </Suspense> */}
        <h1 className="text-2xl flex justify-center font-semibold mt-10">
          To-Do List
        </h1>
        <div className="my-4  mx-4 px-6  py-6 grid grid-rows-2 grid-flow-col gap-x-8 gap-y-11 w-100">
          <div className="flex flex-col items-center justify-start w-90 md:w-md lg:w-100 mx-10 px-10 ">
            <p className="font-semibold">On going Task</p>
            {data.list_data?.length > 0 &&
              (data?.list_data as any)?.map((item: any) => (
                <div
                  key={item?.id}
                  className="flex justify-between w-full rounded overflow-hidden shadow-lg px-2 py-4"
                >
                  <div className="px-6 pt-4 py-2">
                    <div className="flex flex-col ">
                      <span className="inline-block bg-green-200 rounded-full px-2 py-1 text-[12px] w-[80px] text-gray-700 mr-2 mb-2">
                        {item?.status_task === 1 ? `#On Going` : "#Done"}
                      </span>
                      <span className="font-semibold text-sm mb-1 text-wrap">
                        {item.title}
                      </span>
                      <span className="text-[10px]">
                        {moment(item?.createdAt).format("DD MMMM YYYY LT")}
                      </span>
                    </div>
                  </div>
                  <div className="flex justify-center items-center px-2 pt-4">
                    <button
                      onClick={async () => {
                        await deleteTask(item?.id);
                      }}
                      className="inline-block bg-red-500 hover:bg-red-700 rounded-full px-2 py-2 text-sm font-semibold text-white mr-2 mb-2"
                    >
                      <FaTrash />
                    </button>
                    <button
                      onClick={async () => {
                        await updateStatusTask(item?.id);
                      }}
                      className="inline-block bg-green-500 hover:bg-green-700 rounded-full px-2 py-2 text-sm font-semibold text-white mr-2 mb-2"
                    >
                      <FaRegCheckCircle />
                    </button>
                    <button
                      onClick={async () => {
                        const res = await getTaskId(item?.id);
                        if (res.status !== 200) {
                          Swal.fire("error", "Data Not Found", "error");
                        }
                        setId(res?.data?.id);
                        setvalueTitle(res?.data?.title);
                        setMode("edit");
                      }}
                      className="inline-block bg-yellow-500 hover:bg-yellow-700 rounded-full px-2 py-2 text-sm font-semibold text-white mr-2 mb-2"
                    >
                      <FaPencilAlt />
                    </button>
                  </div>
                </div>
              ))}
          </div>
          <div className="flex flex-col items-center justify-start w-90 md:w-md lg:w-100 mx-10 px-10">
            <p className="font-semibold">Completed Task</p>
            {data.list_data_done?.length > 0 &&
              (data?.list_data_done as any)?.map((item: any) => (
                <>
                  <div
                    key={item?.id}
                    className="flex justify-between w-full min-w-sm rounded overflow-hidden shadow-lg px-2 py-4 gap-5"
                  >
                    <div className="px-6 pt-4 py-2">
                      <div className="flex flex-col ">
                        <span className="inline-block bg-red-200 rounded-full px-2 py-1 text-[12px] w-[85px]  text-gray-700 mr-2 mb-2">
                          {item?.status_task === 1 ? `#On Going` : "#Completed"}
                        </span>
                        <span className="font-semibold text-sm mb-1 line-through">
                          {item.title}
                        </span>
                        <span className="text-[10px]">
                          {moment(item?.createdAt).format("DD MMMM YYYY LT")}
                        </span>
                      </div>
                    </div>
                    <div className="flex justify-center items-center px-2 pt-4">
                      <button
                        onClick={async () => {
                          await deleteTask(item?.id);
                        }}
                        className="inline-block bg-red-500 hover:bg-red-700 rounded-full px-2 py-2 text-sm font-semibold text-white mr-2 mb-2"
                      >
                        <FaTrash />
                      </button>
                    </div>
                  </div>
                </>
              ))}
          </div>
        </div>
      </div>
    </>
  );
}
