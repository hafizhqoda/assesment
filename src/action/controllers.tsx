"use server";

import { revalidatePath } from "next/cache";
import { NextResponse } from "next/server";
import { env } from "process";

export const addTask = async (formData: FormData) => {
  try {
    const title = formData.get("title");
    const res = await fetch(`${env.NEXTAUTH_URL}/api/task/create`, {
      method: "POST",
      body: JSON.stringify({
        title: title as string,
      }),
    });
    revalidatePath("/task");
    const ress = await res.json();
    return { ...ress };
  } catch (error) {}
};

export const getTask = async () => {
  try {
    const res = await fetch(`${env.NEXTAUTH_URL}/api/task/getData`);

    revalidatePath("/task");
    // console.log(await res.json());
    const ress = await res.json();
    return { ...ress };
  } catch (error) {}
};
export const getTaskId = async (id: any) => {
  try {
    const res = await fetch(`${env.NEXTAUTH_URL}/api/task/getDataId`, {
      method: "POST",
      body: JSON.stringify({
        id: id as string,
      }),
    });
    revalidatePath("/task");
    // console.log(await res.json());
    const ress = await res.json();
    return { ...ress };
  } catch (error) {}
};

export const updateTask = async (id: number, title: string, e?: any) => {
  console.log(title);
  const res = await fetch(`${env.NEXTAUTH_URL}/api/task/update`, {
    method: "POST",
    body: JSON.stringify({
      id: id,
      title: title,
    }),
  });
  let ress = await res.json();
  revalidatePath("/task");

  // console.log(form);
  return { ress };
};

export const deleteTask = async (id: number, e?: any) => {
  const res = await fetch(`${env.NEXTAUTH_URL}/api/task/delete`, {
    method: "POST",
    body: JSON.stringify({
      id: id,
    }),
  });
  revalidatePath("/task");

  // console.log(form);
  // return { ress };
};
export const updateStatusTask = async (id: number, e?: any) => {
  const res = await fetch(`${env.NEXTAUTH_URL}/api/task/update-status`, {
    method: "POST",
    body: JSON.stringify({
      id: id,
    }),
  });
  revalidatePath("/task");

  // console.log(form);
  // return { ress };
};
