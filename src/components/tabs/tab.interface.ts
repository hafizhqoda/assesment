export type HeaderTab = {
  id: number;
  title: string | JSX.Element;
  icon: any;
  onClick: (params: { id: number; idActive: number }) => any;
};
export interface TabProps extends React.PropsWithChildren {
  listHeader: Array<HeaderTab>;
  activeId?: number;
  manualActive?: boolean;
  onChangeTab?(idActved: number): any;
  onClickTab?: (params: { id: number; idActive: number }) => any;
  headerType?: "Witicon" | "Flat";
  withFooter?: boolean;
  className?: React.HTMLAttributes<HTMLDivElement>["className"];
}
