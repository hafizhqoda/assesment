"use client";

import classNames from "classnames";
import {
  HTMLAttributes,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import { HeaderTab, TabProps } from "./tab.interface";
import Card from "../card/card";
// import { useRouter } from "next/router";
import React from "react";

const Menu = [
  {
    id: 1,
    title: "Lorem ipsum amet.",
    content: "haha hihi hehe",
    color: "text-pink-600",
  },
  {
    id: 2,
    title: "Lorem ipsum amet.",
    content: "haha hihi hehe",
    color: "text-pink-600",
  },
];
function Tab({ withFooter = true, ...props }: TabProps) {
  const [actived, setActived] = useState<number>(0);
  useEffect(
    function () {
      if (props.activeId) {
        setActived((vx) => {
          if (props.listHeader.some((v) => v.id === props.activeId))
            return props.activeId || vx;
          else return vx;
        });
      }
    },
    [props.activeId]
  );
  const changedActive = useCallback(
    (n: number) => {
      props.onChangeTab?.(n);
      setActived(n);
    },
    [setActived]
  );
  const lastHeader = useMemo(
    () =>
      props.listHeader.length
        ? props.listHeader[props.listHeader.length - 1]
        : undefined,
    [props.listHeader]
  );
  const firstHeader = useMemo(
    () => (props.listHeader.length ? props.listHeader[0] : undefined),
    [props.listHeader]
  );
  const nextTab = useCallback(
    function () {
      setActived((vx) => {
        let keyCurrent = props.listHeader.findIndex((v) => v.id === vx),
          newId = vx;
        if (lastHeader?.id !== vx) {
          let newHeader = props.listHeader[keyCurrent + 1];
          newId = newHeader.id;
        }
        props.onChangeTab?.(newId);
        return newId;
      });
    },
    [setActived, props.listHeader]
  );
  const prevTab = useCallback(
    function () {
      setActived((vx) => {
        let keyCurrent = props.listHeader.findIndex((v) => v.id === vx),
          newId = vx;
        if (firstHeader?.id !== vx) {
          let newHeader = props.listHeader[keyCurrent - 1];
          newId = newHeader.id;
        }
        props.onChangeTab?.(newId);
        return newId;
      });
    },
    [setActived, props.listHeader]
  );

  function kEvent(e: KeyboardEvent) {
    if (e.ctrlKey) {
      if (e.keyCode === 39) nextTab();
      if (e.keyCode === 37) prevTab();
    }
  }
  useEffect(
    function () {
      if (props.manualActive) setActived(props.activeId || 0);
    },
    [props.activeId]
  );
  const resetTab = useCallback(
    function () {
      setActived(firstHeader?.id || 0);
      props.onChangeTab?.(firstHeader?.id || 0);
    },
    [props.onChangeTab, setActived, firstHeader]
  );
  useEffect(function () {
    addEventListener("keyup", kEvent);
    return () => removeEventListener("keyup", kEvent);
  }, []);

  // useEffect(function () {
  //   const _push = router.push.bind(router);
  //   router.push = (href, options) => {
  //     _push(href, options);
  //   };
  //   router.events.on("routeChangeStart", resetTab);
  //   return () => router.events.off("routeChangeStart", resetTab);
  //   // setActived(firstHeader?.id || 0)
  //   // props.onChangeTab?.(firstHeader?.id || 0)
  // }, []);
  return (
    <>
      <Card className={classNames(props.className)}>
        {props.headerType !== "Flat" && (
          <Card.Header>
            <div className="w-full">
              <dl className="flex space-x-10 justify-center items-center align-middle">
                {props.listHeader.map(function (
                  itemHead: HeaderTab,
                  i: number
                ) {
                  return (
                    <dt
                      key={i}
                      className={classNames(" cursor-pointer", {
                        "text-sidebar": itemHead.id === actived,
                      })}
                      onClick={() => {
                        !props.manualActive && changedActive(itemHead.id);
                        props.onClickTab?.({
                          id: itemHead.id,
                          idActive: actived,
                        });
                        itemHead.onClick({
                          id: itemHead.id,
                          idActive: actived,
                        });
                      }}
                    >
                      <div
                        className={classNames(
                          `
                              
                              text-[10pt] border
                              border-solid flex 
                              justify-center items-center 
                              p-[5px] mx-auto w-fit rounded-full
                           `,
                          {
                            "border-sidebar": itemHead.id === actived,
                          }
                        )}
                      >
                        {itemHead.icon}
                      </div>
                      <span className=" font-bold text-[8pt] max-md:hidden">
                        {itemHead.title}
                      </span>
                    </dt>
                  );
                })}
              </dl>
            </div>
          </Card.Header>
        )}
        <Card.Body>
          {props.headerType === "Flat" && (
            <div className="w-full">
              <dl className="flex border-b flex-wrap px-3">
                {props.listHeader.map(function (
                  itemHead: HeaderTab,
                  i: number
                ) {
                  return (
                    <dt
                      key={i}
                      className={classNames(
                        " cursor-pointer pl-0 pr-2 min-w-max mb-3 mr-4 px-3",
                        {
                          "text-sidebar": itemHead.id === actived,
                          "border-b-2 border-blue-500": itemHead.id === actived,
                        }
                      )}
                      onClick={() => {
                        !props.manualActive && changedActive(itemHead.id);
                        props.onClickTab?.({
                          id: itemHead.id,
                          idActive: actived,
                        });
                        itemHead.onClick({
                          id: itemHead.id,
                          idActive: actived,
                        });
                      }}
                    >
                      <h4 className="text-slate-900 font-bold text-[19pt] max-md:text-[12pt] flex">
                        {itemHead.title}
                      </h4>
                    </dt>
                  );
                })}
              </dl>
            </div>
          )}
          <div
            className={classNames("grid grid-flow-row auto-rows-max mt-2 px-3")}
          >
            <div>
              {React.Children.map(
                props.children,
                (child, i) =>
                  ((child as { props: {} })?.props as HTMLElement)?.id ===
                    "main-header" &&
                  React.cloneElement((child as JSX.Element) || <></>)
              )}
            </div>
          </div>
          <div className=" grid-flow-row auto-rows-max px-3 w-[100%] py-4">
            {React.Children.map(props.children, (child, i) => {
              if (
                ((child as { props: {} })?.props as HTMLElement)?.id ===
                  "step" &&
                parseInt(
                  (
                    (child as { props: {} })
                      .props as HTMLAttributes<HTMLDivElement> & {
                      itemID: string;
                    }
                  ).itemID
                ) === actived
              )
                return React.cloneElement(child as JSX.Element);
            })}
          </div>
        </Card.Body>
      </Card>
    </>
  );
}

export default Tab;
