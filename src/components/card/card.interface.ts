import React from 'react';

type sizeCardType =  string | number

export interface CardProps extends React.PropsWithChildren, React.HTMLAttributes<HTMLDivElement> {
   size? : sizeCardType
}

export interface CardHeaderProps extends React.PropsWithChildren, React.HTMLAttributes<HTMLDivElement>  {
}

export interface CardFooterProps extends React.PropsWithChildren, React.HTMLAttributes<HTMLDivElement>  {
}

export interface CardBodyProps extends React.PropsWithChildren, React.HTMLAttributes<HTMLDivElement>  {
}