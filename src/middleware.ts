import { NextRequest, NextResponse } from "next/server";

// This function can be marked `async` if using `await` inside
// export function middleware(request: NextRequest) {
//   return NextResponse.redirect(new URL("/login", request.url));
// }
export { default } from "next-auth/middleware";
// See "Matching Paths" below to learn more
export const config = {
  matcher: ["/task", "/api"],
};
